import {useState, useEffect} from 'react'
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router} from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import AddProduct from './pages/AddProduct';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Error from './pages/Error';
import Profile from './pages/Profile';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    const unsetUser = () => {
      localStorage.clear();
    }

    useEffect(() => {

      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token') }`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        if (typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });

        } else {

          setUser({
            id: null,
            isAdmin: null
          });

        }

      })

      }, []);

    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user])

  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <Container fluid>
            <AppNavbar/>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId" element={<ProductView/>}/>
              <Route path="/addProduct" element={<AddProduct/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="/*" element={<Error/>} />
              <Route path="/profile" element={<Profile/>}/>
            </Routes>
            <Footer/>
          </Container>
      </Router>
    </UserProvider>

  )
};

export default App;
