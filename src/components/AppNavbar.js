import {useContext} from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import logoImage from '../images/logo2.jpg';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	console.log(user);


	return(

		<Navbar expand="lg" className="navbar">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">
                	<span style={{ color: 'darkorange' }}>EarTunes</span>
                </Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as={NavLink} to="/" >Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/products" >Products</Nav.Link>
		            
		            {
		            	(user.id !== null && user.isAdmin === true) ?
		            	  <>
		            	  	<Nav.Link as={NavLink} to="/addProduct" >Add Products</Nav.Link>
		            		<Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>	            		
		            	  </>
		            	  :
		            	(user.id !== null) ?
		            	  <>
		            	  	<Nav.Link as={NavLink} to="/profile" >Profile</Nav.Link>
		            		<Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>	            		
		            	  </>
		            	  :
		            	  <>
		            	  	<Nav.Link as={NavLink} to="/login" >Login</Nav.Link>
		            	  	<Nav.Link as={NavLink} to="/register" >Register</Nav.Link>
		            	  </>
		            }

		          </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>

	)

};