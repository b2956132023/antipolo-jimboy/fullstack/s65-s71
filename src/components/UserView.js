import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';
import SearchByPriceRange from './SearchByPriceRange';

export default function UserView({productsData}) {

	const [products, setProducts] = useState([])

	useEffect(() => {
		const productsArr = productsData.map(product => {

			if(product.isActive === true) {
				return (
					<ProductCard productProp={product} key={product._id}/>
					)
			} else {
				return null;
			}
		})

		//set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
		setProducts(productsArr)

	}, [productsData])

	return(
		<>
			<ProductSearch />
			<hr></hr>
			<SearchByPriceRange />
			<hr></hr>
			<h1 className="my-5">Products</h1>
			{ products }
		</>
	)
}
