import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

console.log(productProp);

// Destructured the courseProp object to access its properties
const { _id, name, description, price } = productProp;

    return(
    
      
          <Card className='productCard'>
            <Card.Body className='content'>
              <Card.Title className='title'>{name}</Card.Title>
              <Card.Text className='description'>{description}</Card.Text>
              <Card.Text className='price'>Php {price}</Card.Text>
              <Button as={Link} to={`/products/${_id}`} variant='primary'>View Details</Button>
            </Card.Body>
          </Card>
        
    )
};