import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import logoImage from '../images/logo2.jpg';

export default function Banner({data}) {

	console.log(data);

	const {title, content, destination, label} = data;

	return (


		<Row className="banner">
			<Col className="p-5">
				<img src={logoImage} alt="Logo" style={{ marginBottom: '20px', width: '150px', height: '150px', borderRadius: '50%' }} />
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn btn-primary" to={destination} >{label}</Link>
			</Col>
		</Row>
	);
};
