import { Link } from 'react-router-dom';

export default function Footer() {
  return (
    <div className="footer-container">
      <footer className="footer text-center">
        <div className="footer-link row justify-content-center">
          <p className="homeLink">
            <Link to="/">Home</Link>
          </p>
          <p className="productsLink">
            <Link to="/products">Products</Link>
          </p>
        </div>
        <p>EarTunes &copy; 2023</p>
      </footer>
    </div>
  );
}
