import React, { useState } from 'react';
import { Button } from 'react-bootstrap';

const ProductSearchByPrice = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ minPrice, maxPrice }),
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      console.log(data);
      setSearchResults(data);
      console.log(searchResults);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  return (
    <div className="container my-5">
      <h2>Search Product by Price Range</h2>
      <div className="row">
        <div className="col-3">
          <input style={{ border: '2px solid black' }}
            type="number"
            className="form-control"
            placeholder="Min Price"
            value={minPrice}
            onChange={(e) => setMinPrice(e.target.value)}
          />
        </div>
        <div className="col-3">
          <input style={{ border: '2px solid black' }}
            type="number"
            className="form-control"
            placeholder="Max Price"
            value={maxPrice}
            onChange={(e) => setMaxPrice(e.target.value)}
          />
        </div>
        <div className="col-3">
          <Button className="btn btn-primary" onClick={handleSearch}>
            Search
          </Button>
        </div>
      </div>
      {searchResults.length > 0 && (
        <div className="mt-4">
          <h4>Search Results:</h4>
          <ul className="list-group">
            {searchResults.map((product) => (
              <li key={product.id} className="list-group-item">
                {product.name} - Php {product.price}
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default ProductSearchByPrice;