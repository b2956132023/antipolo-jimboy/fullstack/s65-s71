import React, { useState } from 'react';

const UpdateProfile = ({fetchDetails}) => {
const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
const [email, setEmail] = useState('');
const [message, setMessage] = useState('');

const handleUpdate = async () => {
const token = localStorage.getItem('token');

  try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          firstName,
          lastName,
          email,
        }),
      });

      if (!response.ok) {
        throw new Error('Error updating profile.');
        fetchDetails();
      }

      setMessage('Profile updated successfully!');
      fetchDetails();
    } catch (error) {
      setMessage('Error updating profile.');
      console.error(error);
    }
  };

  return (
    <div className="container mt-4">
      <h2>Update Profile</h2>
      <div className="form-group">
        <label htmlFor="firstName">First Name</label>
        <input
          type="text"
          className="form-control"
          id="firstName"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="lastName">Last Name</label>
        <input
          type="text"
          className="form-control"
          id="lastName"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </div>
      <div className="form-group mb-3">
        <label htmlFor="email">Email</label>
        <input
          type="text"
          className="form-control"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleUpdate}>
        Update Profile
      </button>
      {message && <div className="mt-2">{message}</div>}
    </div>
  );
};

export default UpdateProfile;
