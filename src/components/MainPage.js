import { Button, Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function Home(props) {

	const {title, message} = props;

	return (

		<Row>
            <Col className="p-5">
                <h1>{title}</h1>
                <p>{message}</p>
                <Button variant="primary">Shop Now!</Button>
            </Col>
        </Row>
	)
};