import {Container, Row, Col, Card} from 'react-bootstrap';


export default function Highlights() {

	return (

		<Container>
			<Row>
				<Col>
				<Card className='card-highlights'>
				    <Card.Body>
				      <div>
					  <Card.Title className='title'>Buy and Sell Online</Card.Title>
				      <Card.Text className='description'>
				        EarTunes is a fun, free, and trusted way to buy and sell instantly online.
				      </Card.Text> 
					  </div>
				    </Card.Body>
				  </Card>
				</Col>
				<Col>
					<Card className='card-highlights'>
						<Card.Body>
						<div>
						<Card.Title className='title'>Enjoy Special Deals, Sales, Promos, and Discounts</Card.Title>
						<Card.Text className='description'>
							Doing your online shopping is not only easy and safe, but it's also tons of fun.
						</Card.Text> 
						</div>
						</Card.Body>
					</Card>
				</Col>
				<Col>
					<Card className='card-highlights'>
						<Card.Body>
						<div>
						<Card.Title className='title'>Get Free Shipping</Card.Title>
						<Card.Text className='description'>
							Aside from New User vouchers, you can also enjoy free shipping vouchers today!
						</Card.Text>
						</div>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)

}