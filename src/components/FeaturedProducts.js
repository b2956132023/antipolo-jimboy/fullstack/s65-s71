import { useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import PreviewProducts from './PreviewProducts';

export default function FeaturedProducts() {
  const [previews, setPreviews] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        // To be used to store random numbers and featured product data
        const numbers = [];
        const featured = [];

        const generateRandomNums = () => {
          let randomNum = Math.floor(Math.random() * data.length);

          if (numbers.indexOf(randomNum) === -1) {
            numbers.push(randomNum);
          } else {
            generateRandomNums();
          }
        };

        for (let i = 0; i < 5; i++) {
          generateRandomNums();

          featured.push(
            <Col xs={12} sm={6} md={4} lg={3} xl={2} key={i} className="m-1">
              <PreviewProducts data={data[numbers[i]]} breakPoint={2} />
            </Col>
          );
        }

        setPreviews(featured);
      });
  }, []);

  return (
    <div className="featuredProducts">
      <h2 className="text-center">Featured Products</h2>
      <Row>{previews}</Row>
    </div>
  );
}
