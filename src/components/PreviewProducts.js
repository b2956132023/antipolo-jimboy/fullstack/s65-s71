import {Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Product(props) {

  const { breakPoint, data } = props

  const {_id, name, description, price} = data

  return(
    <Row className='justify-content-center'>
    <Col xs={12} md={breakPoint} className="m-2">
    <Card className='productCard'>
      <div className='content'>
        <Card.Body>
        <Card.Title className='title'>
          <Link to={`/products/${_id}`}>{name}</Link>
        </Card.Title>
        <Card.Text className='description'>{description}</Card.Text>  
        </Card.Body>

        <Card.Footer>
          <h5 className="price">Php {price}</h5>
          <Link to={`/products/${_id}`} className="btn btn-primary d-block">Details</Link>
        </Card.Footer>
      </div>
    </Card>
    </Col>
    </Row>
  )
}