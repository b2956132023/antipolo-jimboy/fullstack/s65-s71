import { useState, useEffect, useContext } from 'react';
import { Container, Card, Form, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function ProductView() {

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const {productId} = useParams();

	const { user } = useContext(UserContext);

	// an object with methods to redirect the user
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const [totalAmount, setTotalAmount] = useState(0);

	const decreaseQuantity = () => {
	  if (quantity > 1) {
	    setQuantity(quantity - 1);
	  }
	};

	const increaseQuantity = () => {
	    setQuantity(quantity + 1);
	};

	useEffect(()=>{
		console.log(productId)

		// a fetch request that will retrieve the details of the specific product
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			
			setTotalAmount(quantity * data.price);
		})
	}, [productId, quantity])


	const order = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL }/users/checkout`, {
			method: "POST",
			headers: {
			  "Content-Type": "application/json",
			  Authorization: `Bearer ${localStorage.getItem('token') }`
			},
			body: JSON.stringify({
		  		productId: productId,
		  		name: name,
		  		quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

		console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully ordered",
					icon: "success",
					text: "You have successfully ordered this product."
				})

				// Allow us to navigate the user back to the course page progammatically instead of using component.
				navigate("/products")
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		});

	}

	return(
		<Container className="mt-5">
			<Row className='justify-content-center'>
				<div className="col-md-4">
					<Card className='productCard'>
						<div className='content'>
							<Card.Body className="text-center">
							<Card.Title className='title'>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text className='description'>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text className='price'>PhP {price}</Card.Text>
							<Form.Group controlId="Quantity">
								<Form.Label>Quantity</Form.Label>
								<div className="d-flex align-items-center">
								<Button variant="outline-secondary" onClick={decreaseQuantity}>-</Button>
								<Form.Control
									style={{ border: '2px solid black' }}
									type="number"
									placeholder="Enter Quantity"
									required
									value={quantity}
									onChange={(e) => setQuantity(e.target.value)}
								/>
								<Button variant="outline-secondary" onClick={increaseQuantity}>+</Button>
								</div>
							</Form.Group>
							<Card.Subtitle>Total Amount:</Card.Subtitle>
							<Card.Text>PhP {totalAmount}</Card.Text>
							{user.id !== null ?
								<Button variant="primary" onClick={() => order(productId)}>Checkout</Button>
							:
								<Button as={Link} to="/login" variant="primary">Log in to order</Button>
							}
							
							</Card.Body>
						</div>
					</Card>
				</div>
			</Row>
		</Container>
	)
}