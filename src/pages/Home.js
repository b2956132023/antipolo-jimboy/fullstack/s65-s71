import Banner from '../components/Banner';
import Hightlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';


export default function Home() {

	const data = {
		title: "EarTunes",
		content: "Where Music Meets Your Ears: Unleash the Soundscapes!",
		destination: "/products",
		label: "Shop now!"
	}

	return (
		<>
			<Banner data={data}/>
			<FeaturedProducts/>
			<Hightlights/>
		</>
	)

};
