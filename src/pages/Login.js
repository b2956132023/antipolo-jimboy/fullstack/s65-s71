import { useState, useEffect, useContext } from "react";   
import { Form, Button } from "react-bootstrap";
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [ isActive, setIsActive ] = useState(false);

    console.log(email);
    console.log(password);

    function authenticateUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data.access) {

                localStorage.setItem("token", data.access);

                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Lazapee"
                });
            } else {

                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your login details and try again."
                });
            }
        })
    };

    const retrieveUserDetails = (token) =>{

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        if((email !== "") && (password !== "") && (email !== null) && (password !== null)) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
        
    }, [email, password]);
    

    return (
        
        (user.id !== null) ?
            <Navigate to="/products" />
            :
                <Form onSubmit={e => authenticateUser(e)} className="formContainer">
                    <div className="input-container">
                       <div className="input-content">
                            <div className="input-dist">
                            <span className="titleTEXT mb-3">Login</span>
                                <div className="input-type">
                                <Form.Group controlId="Email Address">
                                    <Form.Control
                                        className="input-is"
                                        type="email" 
                                        placeholder="Email" 
                                        required
                                        value={email}
                                        onChange={e => {setEmail(e.target.value)}}
                                    />
                                </Form.Group>

                                <Form.Group className="user-box mb-4" controlId="Password1">
                                    <Form.Control
                                        className="input-is"
                                        type="password" 
                                        placeholder="Password" 
                                        required 
                                        value={password}
                                        onChange={e => {setPassword(e.target.value)}}
                                    />
                                </Form.Group>
                                </div>
                                {
                                    isActive
                                    ? <Button type='login'>Submit</Button>
                                    : <Button type='login' disabled>Submit</Button>
                                }
                            </div>
                        
                            
                        </div>                
                    </div>
                    <p className="mt-4">Don't have an account? <a href="/register" className="a2">Sign up!</a></p>
                  
                </Form>

    )
};