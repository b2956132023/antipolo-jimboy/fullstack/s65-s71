import {useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import ArchiveProduct from '../components/ArchiveProduct';

export default function Products() {

	const { user } = useContext(UserContext);

	// State that will be used to store course retrieved from the database.
	const [products, setProducts] = useState([]);

	// Retrieves the courses from the database upon initial render of the "Courses" component
	
	const fetchData = () => {
		// use the route /all to get all active and not active courses (make sure that this route at the backend doesn't have jwt)
		//The fetch will be used to pass the data to Userview and Adminview, where:
			//Userview only active courses will be shown
			//Adminview shows all active and non-active courses
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			// // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components.
			// setProducts(data.map(course =>{
			// 	return(
			// 		<CourseCard key={course._id} courseProp={course} />
			// 	)
			// }))
			setProducts(data);
		});

	}

	useEffect(() => {
		fetchData();
	}, []);


	return(
		<>
		    {
		    	(user.isAdmin === true) 
		    		?
		    		<AdminView productsData={products} fetchData={fetchData} ArchiveProducts={ArchiveProduct} />
		    		:
		    		<UserView productsData={products} />
			}
			
		</>
	)

};
