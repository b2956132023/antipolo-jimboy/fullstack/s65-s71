import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function Register() {

	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const [ isActive, setIsActive] = useState(false);

	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(password);
	console.log(confirmPassword);


	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				alert("Duplicate email found!")
			} else {

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data) {

						setFirstName("");
						setLastName("");
						setEmail("");
						setPassword("");
						setConfirmPassword("");

						alert("Thank you for registering!")

					} else {

						alert("Please try again later.")
					}

				})
			}


		})

	};


	useEffect(() => {

		if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)) {
				setIsActive(true)
		} else {

				setIsActive(false)
		}

	}, [firstName, lastName, email, password, confirmPassword]);



	return (

		(user.id !== null)
		? <Navigate to = "/products"/>
		:<Form onSubmit={e => registerUser(e)} className="formContainer">
			<div className='input-container'>
				<div className='input-content'>
					<div className='input-dist'>
					<p className="titleTEXT mt-3">Register </p>
					<p className="titleTEXT">Signup now and get full access to our app. </p>
		
					<div className="input-type">
						<Form.Group className="input-is" controlId="First Name">
						<Form.Control 
							type="text" 
							placeholder="Enter First Name" 
							required
							value={firstName}
							onChange={e => {setFirstName(e.target.value)}}
						/>
						</Form.Group>
	
						<Form.Group className="input-is" controlId="Last Name">
						<Form.Control
							type="text" 
							placeholder="Enter Last Name" 
							required
							value={lastName}
							onChange={e => {setLastName(e.target.value)}}
						/>
						</Form.Group>					
					
						<Form.Group className="input-is" controlId="Email address">
							<Form.Control
							type="email" 
							placeholder="name@example.com"  
							required
							value={email}
							onChange={e => {setEmail(e.target.value)}}
							/>
						</Form.Group>
			
						<Form.Group className="input-is" controlId="Password1">
							<Form.Control
							type="password" 
							placeholder="Enter Password" 
							required 
							value={password}
							onChange={e => {setPassword(e.target.value)}}
							/>
						</Form.Group>
			
						<Form.Group className="input-is" controlId="Password2">
							<Form.Control
							type="password" 
							placeholder="Confirm Password" 
							required 
							value={confirmPassword}
							onChange={e => {setConfirmPassword(e.target.value)}}
							/>
						</Form.Group>
					</div>
		
					<div className="input-is">
						{
							isActive 
								? <Button className="submit" type="submit" id="submitBtn">Submit</Button>
								: <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
						}
					</div>
		
						<p className="titleTEXT">Already have an acount ? <a href="/login">Signin</a> </p>
					</div>
				</div>
			</div>

		</Form>

	)

};